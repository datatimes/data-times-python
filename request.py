#This Python file is made to fetch the Data and send it to the front-end VueX App.
#!/usr/bin/env python3

import json
import requests
from flask import Flask
from elasticsearch import Elasticsearch

# Create an app
app = Flask(__name__)

DATATIMES_ES_HOST = 'https://es.thedatatimes.com'
es = Elasticsearch(hosts=DATATIMES_ES_HOST, log='info')
uriQuery = 'http://localhost:9200/graphics/_search?q=pageID:'

def get_document(id):
    return es.get(index="core", id=id)

def associated_key(row, counties):
    for key in counties:
        for element in counties[key]:
            if str(row['County_UD']) == str(element):
                return key

@app.route('/elastic_search_data/<string:docID>')
def get_elastic_search_data(docID):
    res = get_document(docID)
    dataInfo = json.dumps(res['_source'])
    return dataInfo

@app.route('/news_data/<int:pageID>')
def get_news_data_from_es(pageID):
    url = 'http://localhost:9200/investigation_pages/_search?q=pageID:'+str(pageID)
    r = requests.get(url)
    return json.dumps(r.json())

@app.route('/get_graphics_from_es/<int:pageID>')
def get_graphics_from_es(pageID):
    url = uriQuery+str(pageID)
    r = requests.get(url)
    return json.dumps(r.json())

# This tells Flask to send requests for "foifeed_url" to the function below
@app.route('/foifeed_url/<int:dataID>')
def fetch_foifeed_data(dataID):
    data = {1: 'https://www.whatdotheyknow.com/feed/search/asylum%20variety:sent',
    2: 'https://www.whatdotheyknow.com/feed/search/economy%20variety:sent',
    3: 'https://www.whatdotheyknow.com/feed/search/violence%20variety:sent',
    4: 'https://www.whatdotheyknow.com/feed/search/gaeilge%20variety:sent',
    5: 'https://www.whatdotheyknow.com/feed/search/coronavirus%20variety:sent'}
    return(data[dataID])